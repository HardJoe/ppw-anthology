# Jojo's Anthology

[![pipeline status](https://gitlab.com/HardJoe/ppw-anthology/badges/master/pipeline.svg)](https://gitlab.com/HardJoe/ppw-anthology/-/commits/master)
[![coverage report](https://gitlab.com/HardJoe/ppw-anthology/badges/master/coverage.svg)](https://gitlab.com/HardJoe/ppw-anthology/-/commits/master)

Collections of individual assignments for Perancangan dan Pemrograman Web (PPW) course in 2020/2021 Odd Semester. The purpose of this anthology is to show the progress of the author in learning web development from basic HTML/CSS to Django framework.


## General Technology Stack

These are the technologies used in all assignments:
- HTML/CSS
- Bootstrap
- Django
- Jinja
- Gitlab CI/CD
- Heroku


## Stories

Each assignment is called "story" and has its own unique theme. The author is Jojo, a fictional character who is learning web development. The stories are as follows:


### Story 1 - Jojo's Profile

Jojo's Profile contains some information about himself, including his hobbies, educational background, a little description, and also social media. This website shows his progress in learning basic HTML/CSS. This is also his first time using Gitlab CI/CD to deploy a web app to Heroku server. Django framework is used since Heroku cannot host a mere static website.


### Story 4 - Jojo's Adventure

Jojo's Adventure shows his adventure in web designing (Story 2), front end developing (Story 3), and Django introduction. It is another Jojo's profile which shows his ability in professional aspects. You can see what kinds of programming skills, languages, projects, and organizations he's had in recent years. This website implements HTML/CSS with the help of Bootstrap, hence the grid layout. Gitlab CI/CD is still utilized for deployment process.


### Story 5 - COOLYEAH

COOLYEAH is a web application that can be used to organize college schedules. User can add courses that he/she takes in current semester or upcoming semesters. Information such as name, lecturer, term, room, and credits are also shown. User can remove courses provided that there has been some misinput or the course has been passed. Unfortunately, edit course feature has not been implemented in this application. 

In this story, Jojo learns how to use Django model to store data in database. He also learns how to use Django forms to create a form in the frontend.


### Story 6 - EO CEO

EO CEO is an application that can be used to manage events and participants. User inputs a new event in main page form and adds participants in event form. Using Django model relationship, each participant is bound to one event. It implements Django's test driven development (TDD) to make sure that no bugs can occur. Minimum test coverage is 90%.


### Story 7 - JQUEWRYYY

JQUEWRYYY is a web application that displays some accordions containing fun facts about Jojo. It uses jQuery to manipulate the DOM.


### Story 8 - BEWKS

BEWKS is a web application that displays some books. User can find books by keyword. The books are fetched from Google Books API. Jojo learns how to use AJAX to fetch data from an API response and jQuery to manipulate the DOM.


### Story 9 - COOKIE TIME!

COOKIE TIME! is a web application that displays some cookies. User can register and login to see the cookies. This is the first time Jojo uses Django authentication system.
