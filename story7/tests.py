from django.test import Client, TestCase
from django.urls import resolve, reverse

from . import views


class UrlsPathTest(TestCase):
    def setUp(self):
        self.index_page = reverse('story7:index')

    def test_index_page_matches_the_right_path(self):
        self.assertEqual(self.index_page, '/story7/')


class PageTest(TestCase):
    def setUp(self):
        self.index_page = reverse('story7:index')

    def test_index_function(self):
        found = resolve(self.index_page)
        self.assertEqual(found.func, views.index)

    def test_index_page_exists(self):
        response = Client().get(self.index_page)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story7/index.html')
