from django.contrib import messages
from django.shortcuts import render, redirect

from .forms import EventForm, MemberForm
from .models import Event, Member


def index(request):
    if request.method == "POST":
        event_form = EventForm(request.POST)
        member_form = MemberForm(request.POST)
        if event_form.is_valid():
            event_form.save()
            messages.success(request, (f"{request.POST['title']} event added."))
            return redirect('story6:index')
        elif member_form.is_valid():
            member_form.save()
            messages.success(request, (f"Participant named {request.POST['name']} added."))
            return redirect('story6:index')
        else:
            messages.error(request, (f"Invalid input!"))
            return redirect('story6:index')
    else:
        event_form = EventForm()
        member_form = MemberForm()
        events = Event.objects.all()
        members = Member.objects.all()
        is_empty = False
        if not events:
            is_empty = True
        context = {
            'event_form': event_form,
            'member_form': member_form,
            'events': events,
            'members': members,
            'is_empty': is_empty,
        }
        return render(request, 'story6/index.html', context)


def delete_event(request, pk):
    event = Event.objects.get(id=pk)
    event.delete()
    messages.warning(request, (f"{event} event deleted."))
    return redirect('story6:index')


def delete_member(request, pk):
    member = Member.objects.get(id=pk)
    member.delete()
    messages.warning(request, (f"Participant named {member} deleted."))
    return redirect('story6:index')
