from django.test import Client, TestCase
from django.urls import reverse

from . import views


class PageTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.index_url = reverse('home:index')

    def test_index_page_exists(self):
        response = self.client.get(self.index_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'home/index.html')
