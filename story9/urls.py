from django.conf.urls import include
from django.urls import path

from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.index, name='index'),
    path('accounts/', include("django.contrib.auth.urls")),
    path('accounts/register/', views.register, name='register'),
]
