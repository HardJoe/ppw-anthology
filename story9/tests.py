from django.contrib.auth.models import User
from django.test import Client, TestCase
from django.urls import reverse

from . import views


class PageTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.index_url = reverse('story9:index')
        self.register_url = reverse('story9:register')

    def test_index_page_exists(self):
        response = self.client.get(self.index_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9/index.html')

    def test_register_page_exists(self):
        response = self.client.get(self.register_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'registration/register.html')


class LogInTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.credentials = {
            'username': 'testuser',
            'password': 'secret'
        }
        User.objects.create_user(**self.credentials)
        self.login_url = '/story9/accounts/login/'

    def test_login(self):
        # send login data
        response = self.client.post(
            self.login_url,
            self.credentials,
            follow=True
        )
        # should be logged in now
        self.assertTrue(response.context['user'].is_active)


class RegisterTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.credentials = {
            'username': 'newuser',
            'password1': 'test123',
            'password2': 'test123',
        }
        self.register_url = reverse('story9:register')

    def test_register(self):
        response = self.client.post(
            self.register_url,
            self.credentials,
            follow=True
        )
        self.assertTrue(response.context['user'].is_active)
