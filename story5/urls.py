from django.urls import path

from . import views


app_name = "story5"

urlpatterns = [
    path("", views.index, name="index"),
    path("add/", views.add_course, name="add_course"),
    path("details/<int:pk>", views.course_details, name="course_details"),
    path("delete/<int:pk>", views.delete_course, name="delete_course"),
]
