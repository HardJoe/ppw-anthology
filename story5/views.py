from django.contrib import messages
from django.shortcuts import render, redirect

from .forms import CourseForm
from .models import Course


def index(request):
    courses = Course.objects.all()
    empty = False
    if not courses:
        empty = True
    context = {'courses': courses, 'empty': empty}
    return render(request, 'story5/index.html', context)


def add_course(request):
    form = CourseForm()
    if request.method == "POST":
        form = CourseForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, (f"{request.POST['name']} course added."))
        return redirect('story5:index')
    context = {'form': form}
    return render(request, 'story5/add.html', context)


def course_details(request, pk):
    course = Course.objects.get(id=pk)
    context = {'course': course}
    return render(request, 'story5/details.html', context)


def delete_course(request, pk):
    if request.method == "POST":
        course = Course.objects.get(id=pk)
        course.delete()
        messages.success(request, (f"{course} course deleted."))
    return redirect('story5:index')
