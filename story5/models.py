from django.db import models


class Course(models.Model):
    name = models.CharField(max_length=70)
    lecturer = models.CharField(max_length=70)
    sks = models.PositiveIntegerField()
    TERM_CHOICES = [('Odd', 'Odd'), ('Even', 'Even')]
    YEAR_CHOICES = [
        ('2020/2021', '2020/2021'),
        ('2021/2022', '2021/2022'),
        ('2022/2023', '2022/2023'),
        ('2023/2024', '2023/2024'),
    ]
    term = models.CharField(max_length=4, choices=TERM_CHOICES, default='Odd')
    year = models.CharField(max_length=9, choices=YEAR_CHOICES, default='2020/2021')
    room = models.CharField(max_length=30)
    desc = models.CharField(max_length=1000)

    def __str__(self):
        return self.name
