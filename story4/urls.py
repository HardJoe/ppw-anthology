from django.urls import path

from . import views

app_name = 'story4'

urlpatterns = [
    path('', views.index, name='index'),
    path('extras/', views.extras, name='extras'),
]
