from django.shortcuts import render


def index(request):
    context = {"type": "index"}
    return render(request, "story4/index.html", context)


def extras(request):
    context = {"type": "extras"}
    return render(request, "story4/extras.html", context)
