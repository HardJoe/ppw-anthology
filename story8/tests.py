from django.test import Client, TestCase
from django.urls import resolve, reverse

from . import views


class PageTest(TestCase):
    def setUp(self):
        self.index_page = reverse('story8:index')

    def test_index_page_matches_the_right_path(self):
        self.assertEqual(self.index_page, '/story8/')

    def test_index_function(self):
        found = resolve(self.index_page)
        self.assertEqual(found.func, views.index)

    def test_index_page_exists(self):
        response = Client().get(self.index_page)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story8/index.html')


class AjaxTest(TestCase):
    def setUp(self):
        self.data_page = reverse('story8:get_books')

    def test_ajax_GET(self):
        response = Client().get(self.data_page + '?q=Test&page=1')
        self.assertEquals(response.status_code, 200)
        self.assertIn('Test', response.content.decode('utf-8'))
