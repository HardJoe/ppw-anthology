from django.urls import path

from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.index, name='index'),
    path('books/', views.get_books, name='get_books'),
]
