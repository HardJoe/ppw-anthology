import json
import requests

from django.http import JsonResponse
from django.shortcuts import render


def index(request):
    url = 'https://www.googleapis.com/books/v1/volumes?q=otaku'
    response = requests.get(url)
    data = response.json()
    context = {
        'total_items': data['totalItems'],
        'items': data['items'],
    }
    return render(request, 'story8/index.html', context)


def get_books(request):
    url = (
        'https://www.googleapis.com/books/v1/volumes?q='
        + request.GET['q']
        + '+page='
        + request.GET['page']
    )
    response = requests.get(url)
    data = json.loads(response.content)
    return JsonResponse(data, safe=False)
