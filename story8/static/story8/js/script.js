var pageCount = 1;
var keyword = '';

$(document).ready(function () {
  $('#more-btn').hide();
  
  // enter in input triggers the button
  $('#book-form').keyup(function (event) {
    if (event.keyCode === 13) {
      $('#find-btn').click();
    }
  });

  $('#find-btn').click(() => {
    keyword = $('#book-form').val();
    pageCount++;
    if (keyword == '') {
      alert('Please input a valid name.');
    } else {
      $.ajax({
        url: '/story8/books?q=' + keyword + '&page=' + pageCount,
        contentType: 'application/json',
        dataType: 'json',
        success: function (result) {
          $('#book-table tbody tr').remove();
          $('#found-amount').replaceWith(result.totalItems);

          for (i = 0; i < result.items.length; i++) {
            var book = result.items[i];
            try {
              var cover = book.volumeInfo.imageLinks.thumbnail;
            } catch {
              var cover = 'https://i.ibb.co/HzfXhc8/matsuri-edit.jpg';
            }
            var title = book.volumeInfo.title;
            var authors = book.volumeInfo.authors
              ? book.volumeInfo.authors.join(', ')
              : '';
            var publishedDate = book.volumeInfo.publishedDate
              ? book.volumeInfo.publishedDate
              : '';
            var pageCount = book.volumeInfo.pageCount
              ? book.volumeInfo.pageCount
              : '';

            $('#book-table > tbody:last-child').append(`
              <tr>
                <td><img src="${cover}"></td>
                <td>${title}</td>
                <td>${authors}</td>
                <td>${publishedDate}</td>
                <td>${pageCount}</td>
              </tr>
            `);
          }
          $('#more-btn').show();
        },
        error: function (result) {
          alert('Sorry, there was an error.');
        },
      });
    }
  });

  $('#more-btn').click(() => {
    pageCount++;
      $.ajax({
        url: '/story8/books?q=' + keyword + '&page=' + pageCount,
        contentType: 'application/json',
        dataType: 'json',
        success: function (result) {
          for (i = 0; i < result.items.length; i++) {
            var book = result.items[i];
            try {
              var cover = book.volumeInfo.imageLinks.thumbnail;
            } catch {
              var cover = 'https://i.ibb.co/HzfXhc8/matsuri-edit.jpg';
            }
            var title = book.volumeInfo.title;
            var authors = book.volumeInfo.authors
              ? book.volumeInfo.authors.join(', ')
              : '';
            var publishedDate = book.volumeInfo.publishedDate
              ? book.volumeInfo.publishedDate
              : '';
            var pageCount = book.volumeInfo.pageCount
              ? book.volumeInfo.pageCount
              : '';

            $('#book-table > tbody:last-child').append(`
              <tr>
                <td><img src="${cover}"></td>
                <td>${title}</td>
                <td>${authors}</td>
                <td>${publishedDate}</td>
                <td>${pageCount}</td>
              </tr>
            `);
          }
        },
        error: function (result) {
          alert('Sorry, there was an error.');
        },
      });
  });
});
