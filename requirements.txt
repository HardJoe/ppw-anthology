asgiref>=3.3.1
astroid>=2.0.4
certifi>=2020.11.8
chardet>=3.0.4
colorama>=0.3.9
coverage>=5.3
dj-database-url>=0.5.0
Django>=3.1.2
django-crispy-forms>=1.10.0
django-environ>=0.4.4
gunicorn>=20.0.4
idna>=2.6
isort>=4.2.15
lazy-object-proxy>=1.3.1
mccabe>=0.6.1
mock>=4.0.2
Pillow>=8.0.1
psycopg2-binary>=2.8.6
pylint>=1.7.2
pytz>=2020.4
requests>=2.18.4
selenium>=3.5.0
six>=1.10.0
sqlparse>=0.3.1
toml>=0.10.2
typed-ast>=1.4.1
urllib3>=1.26.2
whitenoise>=5.2.0
wrapt>=1.10.11
